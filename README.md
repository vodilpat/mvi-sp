# Semestral work NI-MVI vodilpat

## Milestone 2
- Podrobné zadání SP: Rozpoznávanie hlasových príkazov v zvukovej nahrávke pomocou LSTM (bližšie info report alebo Milesone 1) 
- Data sa stahujú pomocou knižnice
- Pokyny ke spuštění: Pustenie jednotlivých python notebookov



## Milestone 1

Zadanie: 

Rozpoznavanie reči za pomoci RNN, GRU alebo LSTM. 

### Popis: 

Vybral som si dataset https://arxiv.org/abs/1804.03209, ktorý je dostupný pre PyTorch za pomoci: https://pytorch.org/audio/stable/generated/torchaudio.datasets.SPEECHCOMMANDS.html#torchaudio.datasets.SPEECHCOMMANDS  

Jedna sa o zvukové nahrávky o dĺžke približne 1 sekundy kde zaznie jeden z 35 slovných príkazov. 

Tieto príkazy by som chcel klasifikovať za pomoci LSTM, ktorý by mal byť dostatočne “silný” RNN. 

~~V prípade dostatočného času (extra) plánujem: Porovnať RNNs, porovnať koreláciu jednotlivých príkazov v čase alebo vylepšiť model za pomoci CNN, auto-encoder ...~~

Zaťiaľ používam model nasledovný: 

 

class My_model(nn.Module): 

    def __init__(self, input_size, hidden_size=512 , n_output=len(labels), num_layers =3): 

        super().__init__() 

        self.lstm = nn.LSTM(input_size, hidden_size, batch_first=True,num_layers=num_layers) 

        self.fc1 = nn.Linear(hidden_size, n_output) 

    def forward(self, x): 

        global out # expose result 

        out, _ = self.lstm(x) 

        out = self.fc1(out[:, -1, :]) 

        return F.log_softmax(out,dim=-1) 

Ale nepodarilo sa mi vyriešiť spôsob trénovania v skupinách (batch). 

Výsledkom mi lezie odpoveď ale tá je bez extrémne dlhého trénovania a skupinovom trénovaní nepoužiteľná. 

Mojim výsledkom je klasifikácia. Nie je príliš čo ukázať. 

 

Prečítal som si takéto články:  

https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8859780  

https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9268723  

https://www.mdpi.com/2079-9292/11/8/1246  

https://www.spiedigitallibrary.org/conference-proceedings-of-spie/10828/108281B/Music-genre-classification-using-a-hierarchical-long-short-term-memory/10.1117/12.2501763.full?SSO=1  

Prečítal som aj nejaké tie návody: 

https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html  

https://www.analyticsvidhya.com/blog/2021/06/lstm-for-text-classification/ 

Našiel som súťaž spojenú so súťažou:  

https://paperswithcode.com/dataset/speech-commands 

Bohužiaľ modelov tam veľa nie je a nič z nich mi nepomôže v mojom plánovanom modely. 

 
